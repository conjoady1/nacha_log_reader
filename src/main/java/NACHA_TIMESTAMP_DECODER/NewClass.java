/*
 * This program is used to parse the NACHA_MONITOR logs 
 * and compare the time stamps within the logs to determine
 * the time elapsed between being last modified and being moved.
 */
package NACHA_TIMESTAMP_DECODER;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author John.Coady
 */
public class NewClass {
    private static ArrayList<File> fileList = new ArrayList<File>();
    private static Scanner scan;
    private static Stack linuxStack;
    private static Stack windowsStack;
    private static Stack Filenames = new Stack<String>();
   
    public static void main(String[] args) throws FileNotFoundException, IOException{
        initializeFileList(args);
        
        //this will parse the time stamps, compare the times, and report to REPORTING.txt
        //for all files
        for (int i = 0; i < fileList.size(); i++){
            linuxStack = parseLinuxStamp(fileList.get(i));
            windowsStack = parseWindowsStamp(fileList.get(i));
            analyzeReport(linuxStack,windowsStack,Filenames);
            System.out.println("successfully scanned file number: " + (i+1));
        }
        System.out.println("successfuly completed");
        
    }
    
    //this method initializes the arraylist of Files.    
    public static int initializeFileList(String[] args){
       try{
           /*In the File constructor, pass the absolute path for the log file that you want to analyze*/
           for (int i = 0; i < (args.length); i++){
               StringBuilder builder = new StringBuilder();
               builder.append(args[i]);
               fileList.add(new File(builder.toString()));
           }
         }
        catch(Exception e){
            System.out.println("error copying files to Arraylist");
            return -1;
        }
    
        return 0;
    } 
    /*
    this method parses the windows timestamps out, converts them to linux epoch
    and puts them into a stack.
    it also adds all of the file names to the Filenames stack
    */
    public static Stack<String> parseWindowsStamp(File logfile){
        Stack<String> stamps = new Stack<String>();
            try {
                String Line;
                String timeStamp;
                int index;
                scan = new Scanner(logfile);
            
                while(scan.hasNext()){
                   StringBuilder builder = new StringBuilder();
                   Line = scan.nextLine();
                    if (Line.contains(new StringBuilder().append('@'))){
                       Filenames.push(Line);
                       index = Line.indexOf('@');
                       index -= 41;
                       builder.append(Line.substring(index, (index + 12)));
                       builder.append(" 19");
                       timeStamp = builder.toString();
                       stamps.push(timeStamp);
                       builder = null;
                   };
                }
            
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
            }
            scan.close();
            Stack<String> stampreturn = timeConversion(stamps);
        return stampreturn;
        
    }

    //this parses the linux timestamps from the file and puts them in a stack
    public static Stack<String> parseLinuxStamp(File logfile) throws FileNotFoundException{
        Stack<String> stamps = new Stack<String>();
            try {
                String Line;
                String timeStamp;
                int index;
                scan = new Scanner(logfile);
            
                while(scan.hasNext()){
                   Line = scan.nextLine();
                    if (Line.contains(new StringBuilder().append('@'))){
                        index = Line.indexOf('@');
                        index+=2;
                        timeStamp = Line.substring(index, (index + 13));
                        stamps.push(timeStamp);
                    }
                        
                }
            }
            catch (FileNotFoundException ex) {
                Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
            }
            scan.close();
            return stamps;
    }
    //This method converts microsoft time to linux time
    public static Stack<String> timeConversion(Stack firstStack){
        Stack LinuxTimeReturn = new Stack<String>();
        Stack tempStack = new Stack<String>();
        int stackSize = firstStack.size();
    
        //convert the times to something more readable and add them to a temporary stack
        for (int i = 0; i < stackSize; i++){
            String date = (String)firstStack.pop();
            try {
                Long millis = new SimpleDateFormat("MMM dd HH:mm yy").parse(date).getTime();
                date = new StringBuilder().append(millis).toString();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tempStack.push(date);
        }
        //copy the temp stack to the return stack to restore the original order
        
        for (int i = 0; i < stackSize; i++){
            LinuxTimeReturn.push(tempStack.pop());
        }
    
        return LinuxTimeReturn;
        }
    
    

    //determine if the times are spaced out too long. report the time differences to REPORTING.txt
    public static void analyzeReport(Stack start, Stack finish, Stack filenames) throws FileNotFoundException, IOException{
        StringBuilder build = new StringBuilder();
        int size = start.size();
        
        for (int i = 0; i < size; i++){
            String datetime1 = (String)start.pop();
            String datetime2 = (String)finish.pop();
            String filename = (String)filenames.pop();
            Long endepoch = Long.parseLong(datetime1);
            Long beginepoch = Long.parseLong(datetime2);
            int difference = (int)((endepoch - beginepoch)/60000);
            
            //write error message
            if ((endepoch - beginepoch) > 900000){
                build.append("the file named: \n").append(filename);
                build.append("\nhad a delay of ").append(difference).append(" minutes\n");
                System.out.println("found one \n");
            }
            
            
        }
        FileOutputStream FOS = new FileOutputStream("C:\\Users\\john.coady\\Documents\\NetBeansProjects\\mavenproject1\\NACHA_LOG_TIMESTAMP_DECODER\\src\\main\\java\\NACHA_TIMESTAMP_DECODER\\REPORTING.txt",true);
        FOS.write(build.toString().getBytes());
        FOS.close();
        System.out.println(build.toString());
    }
}